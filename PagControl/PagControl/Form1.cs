﻿using Npgsql;
using PagControl.Repositorios;

using PagControl.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PagControl
{
    public partial class Form1 : Form
    {
        private UsuarioRepositorio _UsuarioRepositorio;

        public Form1()
        {
            _UsuarioRepositorio = new UsuarioRepositorio();
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TbEmail.Text) || string.IsNullOrEmpty(TbSenha.Text))
            {
                LblStatus.Text = "Por favor, preencha seus dados";
                LblStatus.Visible = true;
            }
            else
            {
                CredenciaisUsuarioDto credenciais = new CredenciaisUsuarioDto();
                credenciais.Email = TbEmail.Text;
                credenciais.Senha = TbSenha.Text;

                if (_UsuarioRepositorio.Login(credenciais))
                {
                    Hide();
                    MenuPrincipal menuPrincipal = new MenuPrincipal();
                    menuPrincipal.ShowDialog();
                    Close();
                }
                else
                {
                    LblStatus.Text = "Dados não encontrados";
                }
            }
        }
    }
}
