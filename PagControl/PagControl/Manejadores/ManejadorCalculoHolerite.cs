﻿using PagControl.Dto;
using PagControl.Repositorios;
using PagControl.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Manejadores
{
    public class ManejadorCalculoHolerite
    {
        DateTime now = DateTime.Now;

        public int CalcularHolerite(int codigoFuncionario)
        {
            HoleriteDto novoHolerite = new HoleriteDto();

            ImpostoRepositorio impostoRepositorio = new ImpostoRepositorio();
            FuncionarioRepositorio funcionarioRepositorio = new FuncionarioRepositorio();
            LancamentoRepositorio lancamentoRepositorio = new LancamentoRepositorio();
            FaixaImpostoRepositorio faixaImpostoRepositorio = new FaixaImpostoRepositorio();
            HoleriteRepositorio holeriteRepositorio = new HoleriteRepositorio();
            ImpostoAplicadoRepositorio impostoAplicadoRepositorio = new ImpostoAplicadoRepositorio();

            List<ImpostoAplicadoDto> ImpostosAplicados = new List<ImpostoAplicadoDto>();

            var funcionario = funcionarioRepositorio.BuscarFuncionario(codigoFuncionario);
            var impostos = impostoRepositorio.ListarImpostos().OrderBy(x=> x.CodigoImpostoRelacionado == 0).ToList();

            var inicio = new DateTime(now.Year, now.Month, 1);
            var fim = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month));

            //dias totais que o mes atual tem
            var diasTotais = (fim - inicio).Days+1;

            //dias uteis que o mes atual tem
            var diasUteisNoMes = Utilitarios.CalcularDiasUteis(inicio, fim);

            //dias trabalhados no mes
            var diasTrabalhados = lancamentoRepositorio.ListarDiasTrabalhados(inicio, fim, codigoFuncionario);

            var salarioBase = funcionario.Salario;

            var valorDia = salarioBase / diasTotais;

            var diasFaltados = (diasUteisNoMes - diasTrabalhados);
            var valorDiasFaltados = diasFaltados * valorDia;

            novoHolerite.TotalVencimentos = salarioBase - valorDiasFaltados;
            novoHolerite.DiasTrabalhados = diasTotais - diasFaltados;
            novoHolerite.MesReferente = DateTime.Now;
            novoHolerite.CodigoFuncionario = codigoFuncionario;

            impostos.ForEach(x =>
            {
                var impostoRelacionado = ImpostosAplicados.Where(a => a.CodigoImposto == x.CodigoImpostoRelacionado).FirstOrDefault();

                List<FaixaImpostoDto> faixas = faixaImpostoRepositorio.ListarFaixas(x.CodigoImposto);

                for(int i = 0; i <= faixas.Count(); i++)
                {
                    if (x.CodigoImpostoRelacionado == null)
                    {
                        if (i + 1 != faixas.Count())
                        {
                            if (faixas[i].Condicao > novoHolerite.TotalVencimentos)
                            {
                                ImpostoAplicadoDto novoImposto = new ImpostoAplicadoDto();
                                novoImposto.Percentual = faixas[i].ValorFaixa;
                                novoImposto.CodigoImposto = x.CodigoImposto;
                                novoImposto.NomeImposto = x.Nome;

                                if (faixas[i].DeduzirValor != null)
                                    novoImposto.Valor = ((novoHolerite.TotalVencimentos * faixas[i].ValorFaixa) / 100) - faixas[i].DeduzirValor.Value;
                                else
                                    novoImposto.Valor = ((novoHolerite.TotalVencimentos * faixas[i].ValorFaixa) / 100);

                                ImpostosAplicados.Add(novoImposto);
                                break;
                            }
                        }
                        else
                        {
                            ImpostoAplicadoDto novoImposto = new ImpostoAplicadoDto();
                            novoImposto.Percentual = faixas[i].ValorFaixa;
                            novoImposto.CodigoImposto = x.CodigoImposto;
                            novoImposto.NomeImposto = x.Nome;

                            if (faixas[i].DeduzirValor != null)
                                novoImposto.Valor = ((novoHolerite.TotalVencimentos * faixas[i].ValorFaixa) / 100) - faixas[i].DeduzirValor.Value;
                            else
                                novoImposto.Valor = ((novoHolerite.TotalVencimentos * faixas[i].ValorFaixa) / 100);

                            ImpostosAplicados.Add(novoImposto);
                        }
                    }
                    ///CONDIÇÃO ESPECIAL CASO SEJA IRRF
                    else
                    {
                        if (i + 1 != faixas.Count())
                        {
                            if (faixas[i].Condicao > novoHolerite.TotalVencimentos - impostoRelacionado.Valor)
                            {
                                ImpostoAplicadoDto novoImposto = new ImpostoAplicadoDto();
                                novoImposto.Percentual = faixas[i].ValorFaixa;
                                novoImposto.CodigoImposto = x.CodigoImposto;
                                novoImposto.NomeImposto = x.Nome;

                                if (faixas[i].DeduzirValor != null)
                                    novoImposto.Valor = ((novoHolerite.TotalVencimentos - impostoRelacionado.Valor) * faixas[i].ValorFaixa) / 100 - faixas[i].DeduzirValor.Value;
                                else
                                    novoImposto.Valor = ((novoHolerite.TotalVencimentos - impostoRelacionado.Valor )* faixas[i].ValorFaixa) / 100;

                                ImpostosAplicados.Add(novoImposto);
                                break;
                            }
                        }
                    }
                }
            });

            var totalDescontos = ImpostosAplicados.Sum(x => x.Valor);
            novoHolerite.ValorLiquido = novoHolerite.TotalVencimentos - totalDescontos;
            novoHolerite.TotalDescontos = totalDescontos;

            var codHolerite = holeriteRepositorio.CadastrarHolerite(novoHolerite);

            if (codHolerite != 0)
            {
                ImpostosAplicados.ForEach(x =>
                {
                    x.CodigoHolerite = codHolerite;
                });

                if (!impostoAplicadoRepositorio.CadastrarImpostosAplicados(ImpostosAplicados))
                    return 0;
            }

            return codHolerite;
        }
    }
}
