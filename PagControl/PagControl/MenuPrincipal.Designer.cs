﻿namespace PagControl
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.holerites1 = new PagControl.Controls.Holerites();
            this.folhaPagamento2 = new PagControl.Controls.FolhaPagamento();
            this.lancamentoHoras1 = new PagControl.Controls.LancamentoHoras();
            this.registroFuncionario1 = new PagControl.Controls.RegistroFuncionario();
            this.folhaPonto1 = new PagControl.Controls.Relatorios();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(248, 449);
            this.panel1.TabIndex = 0;
            // 
            // button7
            // 
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button7.Location = new System.Drawing.Point(0, 331);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(245, 36);
            this.button7.TabIndex = 7;
            this.button7.Text = "Sair";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button6.Location = new System.Drawing.Point(0, 289);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(245, 36);
            this.button6.TabIndex = 6;
            this.button6.Text = "Folhas de pagamento";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button5.Location = new System.Drawing.Point(3, 247);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(245, 36);
            this.button5.TabIndex = 5;
            this.button5.Text = "Relatórios";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button4.Location = new System.Drawing.Point(3, 205);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(245, 36);
            this.button4.TabIndex = 4;
            this.button4.Text = "Holerites";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button3.Location = new System.Drawing.Point(0, 163);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(245, 36);
            this.button3.TabIndex = 3;
            this.button3.Text = "Projeções";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button2.Location = new System.Drawing.Point(0, 121);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(245, 36);
            this.button2.TabIndex = 2;
            this.button2.Text = "Lançamento de horas";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button1.Location = new System.Drawing.Point(0, 79);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(245, 36);
            this.button1.TabIndex = 1;
            this.button1.Text = "Registro de funcionário";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 42);
            this.label1.TabIndex = 0;
            this.label1.Text = "PagControl";
            // 
            // holerites1
            // 
            this.holerites1.Location = new System.Drawing.Point(251, 0);
            this.holerites1.Name = "holerites1";
            this.holerites1.Size = new System.Drawing.Size(923, 459);
            this.holerites1.TabIndex = 5;
            // 
            // folhaPagamento2
            // 
            this.folhaPagamento2.Location = new System.Drawing.Point(251, 0);
            this.folhaPagamento2.Name = "folhaPagamento2";
            this.folhaPagamento2.Size = new System.Drawing.Size(923, 448);
            this.folhaPagamento2.TabIndex = 4;
            // 
            // lancamentoHoras1
            // 
            this.lancamentoHoras1.Location = new System.Drawing.Point(251, 0);
            this.lancamentoHoras1.Name = "lancamentoHoras1";
            this.lancamentoHoras1.Size = new System.Drawing.Size(923, 448);
            this.lancamentoHoras1.TabIndex = 3;
            // 
            // registroFuncionario1
            // 
            this.registroFuncionario1.Location = new System.Drawing.Point(251, 0);
            this.registroFuncionario1.Name = "registroFuncionario1";
            this.registroFuncionario1.Size = new System.Drawing.Size(923, 448);
            this.registroFuncionario1.TabIndex = 1;
            // 
            // folhaPonto1
            // 
            this.folhaPonto1.Location = new System.Drawing.Point(251, 0);
            this.folhaPonto1.Name = "folhaPonto1";
            this.folhaPonto1.Size = new System.Drawing.Size(923, 459);
            this.folhaPonto1.TabIndex = 6;
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1165, 449);
            this.Controls.Add(this.registroFuncionario1);
            this.Controls.Add(this.folhaPonto1);
            this.Controls.Add(this.holerites1);
            this.Controls.Add(this.folhaPagamento2);
            this.Controls.Add(this.lancamentoHoras1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MenuPrincipal";
            this.Text = "PagControl";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private Controls.RegistroFuncionario registroFuncionario1;
        private Controls.LancamentoHoras lancamentoHoras1;
        private Controls.FolhaPagamento folhaPagamento2;
        private Controls.Holerites holerites1;
        private Controls.Relatorios folhaPonto1;
    }
}