﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using PagControl.Dto;
using PagControl.Repositorios;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Util
{
    public class GeradorRelatorio
    {
        public void GerarRelatorio(DepartamentoDto departamento)
        {
            HoleriteRepositorio holeriteRepositorio = new HoleriteRepositorio();

            var itensRelatorio = holeriteRepositorio.BuscarRelatorio(departamento, DateTime.Now);

            string caminho = @"C:\Users\rafae\Desktop\RELATORIOS\" + string.Format("relatorio_{0}_{1}.pdf", departamento.ToString(), DateTime.Now.ToString("yyyy MMMM"));

            CriarDocumento(itensRelatorio, caminho);
        }

        public void GerarRelatorio()
        {
            HoleriteRepositorio holeriteRepositorio = new HoleriteRepositorio();

            var itensRelatorio = holeriteRepositorio.BuscarRelatorio(DateTime.Now);

            string caminho = @"C:\Users\rafae\Desktop\RELATORIOS\" + string.Format("relatorio_Geral_{0}.pdf", DateTime.Now.ToString("yyyy MMMM"));

            CriarDocumento(itensRelatorio, caminho);
        }

        private void CriarDocumento(List<RelatorioItemDto> itens, string caminho)
        {

            Document doc = new Document(PageSize.A4);
            doc.SetMargins(40, 40, 40, 80);
            doc.AddCreationDate();//adicionando as configuracoes

            PdfPTable table = new PdfPTable(6);
            table.WidthPercentage = 100;
            table.SpacingBefore = 20;

            table.AddCell("Departamento");
            table.AddCell("Funcionario");
            table.AddCell("CPF");
            table.AddCell("Vencimentos");
            table.AddCell("Salario liquido");
            table.AddCell("Descontos");

            foreach (var item in itens)
            {
                table.AddCell(item.Departamento.ToString());
                table.AddCell(item.NomeFuncionario);
                table.AddCell(item.CPF);
                table.AddCell(item.TotalVencimentos.ToString());
                table.AddCell(item.SalarioLiquido.ToString());
                table.AddCell(item.TotalDescontos.ToString());
            };

            PdfPTable tabelatotal = new PdfPTable(6);
            tabelatotal.WidthPercentage = 100;
            tabelatotal.SpacingBefore = 20;
            tabelatotal.DefaultCell.Border = Rectangle.NO_BORDER;

            tabelatotal.AddCell("");
            tabelatotal.AddCell("");
            tabelatotal.AddCell("Total");
            tabelatotal.AddCell("R$ " + itens.Sum(x => x.TotalVencimentos).ToString());
            tabelatotal.AddCell("R$ " + itens.Sum(x => x.SalarioLiquido).ToString());
            tabelatotal.AddCell("R$ " + itens.Sum(x => x.TotalDescontos).ToString());

            //caminho onde sera criado o pdf + nome desejado
            //OBS: o nome sempre deve ser terminado com .pdf

            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(caminho, FileMode.Create));

            doc.Open();

            string dados = "";

            //criando a variavel para paragrafo
            Paragraph cabecalho = new Paragraph(dados,
            new Font(Font.NORMAL, 20, Font.BOLD));
            //etipulando o alinhamneto
            cabecalho.Alignment = Element.ALIGN_JUSTIFIED;
            //Alinhamento Justificado
            //adicioando texto
            cabecalho.Add("Relatório PagControl");

            //criando a variavel para paragrafo
            Paragraph dat = new Paragraph(dados,
            new Font(Font.NORMAL, 16, Font.BOLD));
            //etipulando o alinhamneto
            dat.Alignment = Element.ALIGN_JUSTIFIED;
            //Alinhamento Justificado
            //adicioando texto
            dat.Add(string.Format("Mês: {0} de {1}", DateTime.Now.ToString("MMMM"), DateTime.Now.Year));

            doc.Add(cabecalho);
            doc.Add(dat);
            doc.Add(table);
            doc.Add(tabelatotal);
            doc.Close();
        }
    }
}
