﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using PagControl.Dto;
using PagControl.Repositorios;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Util
{
    public class GeradorDocumentoHolerite
    {
        public void GerarDocumentoHolerite(int codigoHolerite)
        {
            DocumentoHoleriteDto documentoHoleriteDto = new DocumentoHoleriteDto();
            HoleriteRepositorio holeriteRepositorio = new HoleriteRepositorio();
            ImpostoAplicadoRepositorio impostoAplicadoRepositorio = new ImpostoAplicadoRepositorio();

            var holerite = holeriteRepositorio.BuscarHoleriteMaisInfo(codigoHolerite);
            var impostosAplicados = impostoAplicadoRepositorio.ListarImpostosAplicados(codigoHolerite);

            documentoHoleriteDto.CodigoFuncionario = holerite.CodigoFuncionario;
            documentoHoleriteDto.CodigoHolerite = holerite.CodigoHolerite;
            documentoHoleriteDto.DiasTrabalhados = holerite.DiasTrabalhados;
            documentoHoleriteDto.TotalDescontos = holerite.TotalDescontos;
            documentoHoleriteDto.TotalVencimentos = holerite.TotalVencimentos;
            documentoHoleriteDto.ValorLiquido = holerite.ValorLiquido;
            documentoHoleriteDto.MesReferente = holerite.MesReferente;
            documentoHoleriteDto.NomeFuncionario = holerite.NomeFuncionario;
            documentoHoleriteDto.ImpostosAplicados = impostosAplicados;

            Document doc = new Document(PageSize.A4);
            doc.SetMargins(40, 40, 40, 80);
            doc.AddCreationDate();//adicionando as configuracoes

            PdfPTable table = new PdfPTable(4);
            table.WidthPercentage = 100;
            table.SpacingBefore = 20;

            table.AddCell("Descrição");
            table.AddCell("Referência");
            table.AddCell("Vencimentos");
            table.AddCell("Descontos");

            //vencimentos
            table.AddCell("Salario normal");
            table.AddCell(documentoHoleriteDto.DiasTrabalhados.ToString() +" Dias");
            table.AddCell(documentoHoleriteDto.TotalVencimentos.ToString());
            table.AddCell("");

            //impostos

            foreach (var item in documentoHoleriteDto.ImpostosAplicados)
            {
                table.AddCell(item.NomeImposto);
                table.AddCell(item.Percentual.ToString() + "%");
                table.AddCell("");
                table.AddCell(item.Valor.ToString());
            };

            PdfPTable tableTotal = new PdfPTable(4);
            tableTotal.WidthPercentage = 100;
            tableTotal.SpacingBefore = 10;
            tableTotal.DefaultCell.Border = Rectangle.NO_BORDER;

            tableTotal.AddCell("");
            tableTotal.AddCell("");
            tableTotal.AddCell("Total vencimentos");
            tableTotal.AddCell("Total descontos");

            tableTotal.AddCell("");
            tableTotal.AddCell("");
            tableTotal.AddCell(documentoHoleriteDto.TotalVencimentos.ToString());
            tableTotal.AddCell(documentoHoleriteDto.TotalDescontos.ToString());

            tableTotal.AddCell("");
            tableTotal.AddCell("");
            tableTotal.AddCell("");
            tableTotal.AddCell("Valor líquido");

            tableTotal.AddCell("");
            tableTotal.AddCell("");
            tableTotal.AddCell("");
            tableTotal.AddCell(documentoHoleriteDto.ValorLiquido.ToString());

            //caminho onde sera criado o pdf + nome desejado
            //OBS: o nome sempre deve ser terminado com .pdf
            string caminho = @"C:\Users\rafae\Desktop\HOLERITES\" + string.Format("HOLERITE_{0}_{1}.pdf", documentoHoleriteDto.NomeFuncionario, documentoHoleriteDto.MesReferente.ToString("yyyy MMMM"));

            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(caminho, FileMode.Create));

            doc.Open();

            string dados = "";

            //criando a variavel para paragrafo
            Paragraph cabecalho = new Paragraph(dados,
            new Font(Font.NORMAL, 20, Font.BOLD));
            //etipulando o alinhamneto
            cabecalho.Alignment = Element.ALIGN_JUSTIFIED;
            //Alinhamento Justificado
            //adicioando texto
            cabecalho.Add("PagControl");


            //criando a variavel para paragrafo
            Paragraph nomeFunc = new Paragraph(dados,
            new Font(Font.NORMAL, 12));
            //etipulando o alinhamneto
            nomeFunc.Alignment = Element.ALIGN_JUSTIFIED;
            //Alinhamento Justificado
            //adicioando texto
            nomeFunc.Add(string.Format("Funcionario: {0}    Codigo: {1}", documentoHoleriteDto.NomeFuncionario, documentoHoleriteDto.CodigoFuncionario));

            //criando a variavel para paragrafo
            Paragraph dept = new Paragraph(dados,
            new Font(Font.NORMAL, 12));
            //etipulando o alinhamneto
            dept.Alignment = Element.ALIGN_JUSTIFIED;
            //Alinhamento Justificado
            //adicioando texto
            dept.Add("Dept.: "+documentoHoleriteDto.Departamento.ToString());

            //criando a variavel para paragrafo
            Paragraph mesRef = new Paragraph(dados,
            new Font(Font.NORMAL, 12));
            //etipulando o alinhamneto
            mesRef.Alignment = Element.ALIGN_JUSTIFIED;
            //Alinhamento Justificado
            //adicioando texto
            mesRef.Add(string.Format("Mês: {0} de {1}", documentoHoleriteDto.MesReferente.ToString("MMMM"), documentoHoleriteDto.MesReferente.Year));

            PdfContentByte cb = writer.DirectContent;

            cb.SetLineWidth(2.0f);   // Make a bit thicker than 1.0 default
            cb.SetGrayStroke(1); // 1 = black, 0 = white
            float x = 72f;
            float y = 72f;
            cb.MoveTo(x, y);
            cb.LineTo(x + 72f * 6, y);
            cb.Stroke();

            Paragraph empty = new Paragraph(dados,
          new Font(Font.NORMAL, 14));
            //etipulando o alinhamneto
            mesRef.Alignment = Element.ALIGN_JUSTIFIED;
            //Alinhamento Justificado
            //adicioando texto
            mesRef.Add("");

            //acidionado paragrafo ao documento
            doc.Add(cabecalho);
            doc.Add(nomeFunc);
            doc.Add(dept);
            doc.Add(mesRef);
            doc.Add(empty);
            doc.Add(table);
            doc.Add(tableTotal);
            doc.Close();
        }
    }
}
