﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Util
{
    public class Utilitarios
    {
        public static int CalcularDiasUteis(DateTime dataInicio, DateTime dataFim)
        {
            int dias = 0;

            while (dataInicio.Date <= dataFim.Date)
            {
                if (dataInicio.DayOfWeek != DayOfWeek.Saturday
                   && dataInicio.DayOfWeek != DayOfWeek.Sunday)
                    dias++;

                dataInicio = dataInicio.AddDays(1);
            }

            return dias;
        }
    }
}
