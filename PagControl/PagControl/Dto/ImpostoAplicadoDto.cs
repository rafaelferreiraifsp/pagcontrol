﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Dto
{
    public class ImpostoAplicadoDto
    {
        public int Codigo { get; set; }
        public double Valor { get; set; }
        public double Percentual { get; set; }
        public string NomeImposto { get; set; }
        public int CodigoHolerite { get; set; }
        public int CodigoImposto { get; set; }
    }
}
