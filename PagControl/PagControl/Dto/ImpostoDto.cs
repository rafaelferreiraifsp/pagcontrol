﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Dto
{
    public class ImpostoDto
    {
        public int CodigoImposto { get; set; }
        public int? CodigoImpostoRelacionado { get; set; }
        public string Nome { get; set; }
        public bool TemFaixas { get; set; }
    }
}
