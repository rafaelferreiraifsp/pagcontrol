﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Dto
{
    public class LancamentoInputModelDto
    {
        public int CodigoFuncionario { get; set; }
        public DateTime DataLancamento { get; set; }
    }
}
