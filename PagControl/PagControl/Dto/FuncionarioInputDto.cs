﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Dto
{
    public class FuncionarioInputDto
    {
        public string Nome { get; set; }
        public string EmailPessoal { get; set; }
        public string EmailEmpresa{ get; set; }
        public string NomeCracha { get; set; }
        public string Cargo { get; set; }
        public string Cpf { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
        public double Salario { get; set; }
        public string RG { get; set; }
        public DateTime DataNascimento { get; set; }
        public DateTime DataAdmissao { get; set; }
        public DepartamentoDto Departamento { get; set; }
        public SexoDto Sexo { get; set; }
    }
}
