﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Dto
{
    public enum DepartamentoDto
    {
        Financeiro,
        RH,
        Operacional,
        Logistica,
        Vendas
    }
}
