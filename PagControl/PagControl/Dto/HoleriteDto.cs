﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Dto
{
    public class HoleriteDto
    {
        public int CodigoHolerite { get; set; }
        public double TotalDescontos { get; set; }
        public double ValorLiquido { get; set; }
        public double TotalVencimentos { get; set; }
        public int DiasTrabalhados { get; set; }
        public DateTime MesReferente { get; set; }
        public int CodigoFuncionario { get; set; }
    }
}
