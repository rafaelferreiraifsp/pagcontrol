﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Dto
{
    public class FaixaImpostoDto
    {
        public int CodigoFaixa { get; set; }
        public double ValorFaixa { get; set; }
        public double Condicao { get; set; }
        public int CodigoImposto { get; set; }
        public double? DeduzirValor { get; set; }
    }
}
