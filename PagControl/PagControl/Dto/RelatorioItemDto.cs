﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Dto
{
    public class RelatorioItemDto
    {
        public DepartamentoDto Departamento { get; set; }
        public string NomeFuncionario { get; set; }
        public string CPF { get; set; }
        public double TotalVencimentos { get; set; }
        public double SalarioLiquido { get; set; }
        public double TotalDescontos { get; set; }
    }
}
