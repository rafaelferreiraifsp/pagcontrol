﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Dto
{
    public class DocumentoHoleriteDto
    {
        public int CodigoHolerite { get; set; }
        public double TotalDescontos { get; set; }
        public double ValorLiquido { get; set; }
        public double TotalVencimentos { get; set; }
        public string NomeFuncionario { get; set; }
        public int DiasTrabalhados { get; set; }
        public DateTime MesReferente { get; set; }
        public int CodigoFuncionario { get; set; }
        public DepartamentoDto Departamento { get; set; }
        public List<ImpostoAplicadoDto> ImpostosAplicados { get; set; }
    }
}
