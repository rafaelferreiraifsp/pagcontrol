﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Dto
{
    public class CredenciaisUsuarioDto
    {
        public string Email { get; set; }
        public string Senha { get; set; }
    }
}
