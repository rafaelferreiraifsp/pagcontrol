﻿using PagControl.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.App
{
    public static class UsuarioLogado
    {
        public static string NomeUsuario { get; set; }
        public static int CodigoUsuario { get; set; }
        public static DateTime DataExpiracao { get; set; }
        public static Perfil Perfil { get; set; }
    }
}
