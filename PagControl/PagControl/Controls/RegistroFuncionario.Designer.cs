﻿namespace PagControl.Controls
{
    partial class RegistroFuncionario
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TbNome = new System.Windows.Forms.TextBox();
            this.TbCPF = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TbEmail = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TbTelefone = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TbEndereco = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TbSalario = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.DtNascimento = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.DtAdmissao = new System.Windows.Forms.DateTimePicker();
            this.TbRg = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.CbDepartamento = new System.Windows.Forms.ComboBox();
            this.TbCargo = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.TbEmailEmpresa = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.CbSexo = new System.Windows.Forms.ComboBox();
            this.TbCracha = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Registro de funcionário";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nome*";
            // 
            // TbNome
            // 
            this.TbNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbNome.Location = new System.Drawing.Point(149, 69);
            this.TbNome.MaxLength = 30;
            this.TbNome.Name = "TbNome";
            this.TbNome.Size = new System.Drawing.Size(206, 22);
            this.TbNome.TabIndex = 2;
            // 
            // TbCPF
            // 
            this.TbCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbCPF.Location = new System.Drawing.Point(149, 124);
            this.TbCPF.MaxLength = 11;
            this.TbCPF.Name = "TbCPF";
            this.TbCPF.Size = new System.Drawing.Size(206, 22);
            this.TbCPF.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "CPF*";
            // 
            // TbEmail
            // 
            this.TbEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbEmail.Location = new System.Drawing.Point(149, 182);
            this.TbEmail.MaxLength = 30;
            this.TbEmail.Name = "TbEmail";
            this.TbEmail.Size = new System.Drawing.Size(206, 22);
            this.TbEmail.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Email particular*";
            // 
            // TbTelefone
            // 
            this.TbTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbTelefone.Location = new System.Drawing.Point(149, 153);
            this.TbTelefone.MaxLength = 10;
            this.TbTelefone.Name = "TbTelefone";
            this.TbTelefone.Size = new System.Drawing.Size(206, 22);
            this.TbTelefone.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 20);
            this.label5.TabIndex = 7;
            this.label5.Text = "Telefone*";
            // 
            // TbEndereco
            // 
            this.TbEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbEndereco.Location = new System.Drawing.Point(598, 69);
            this.TbEndereco.MaxLength = 40;
            this.TbEndereco.Name = "TbEndereco";
            this.TbEndereco.Size = new System.Drawing.Size(206, 22);
            this.TbEndereco.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(456, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "Endereço*";
            // 
            // TbSalario
            // 
            this.TbSalario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbSalario.Location = new System.Drawing.Point(598, 184);
            this.TbSalario.MaxLength = 10;
            this.TbSalario.Name = "TbSalario";
            this.TbSalario.Size = new System.Drawing.Size(206, 22);
            this.TbSalario.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(456, 184);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 20);
            this.label7.TabIndex = 11;
            this.label7.Text = "Salário base*";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button1.Location = new System.Drawing.Point(666, 409);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(245, 36);
            this.button1.TabIndex = 13;
            this.button1.Text = "Salvar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // DtNascimento
            // 
            this.DtNascimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtNascimento.Location = new System.Drawing.Point(21, 306);
            this.DtNascimento.Name = "DtNascimento";
            this.DtNascimento.Size = new System.Drawing.Size(334, 26);
            this.DtNascimento.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(17, 283);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(152, 20);
            this.label8.TabIndex = 15;
            this.label8.Text = "Data de nascimento";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(17, 350);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(138, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "Data de admissão";
            // 
            // DtAdmissao
            // 
            this.DtAdmissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtAdmissao.Location = new System.Drawing.Point(21, 373);
            this.DtAdmissao.Name = "DtAdmissao";
            this.DtAdmissao.Size = new System.Drawing.Size(334, 26);
            this.DtAdmissao.TabIndex = 16;
            // 
            // TbRg
            // 
            this.TbRg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbRg.Location = new System.Drawing.Point(149, 97);
            this.TbRg.MaxLength = 10;
            this.TbRg.Name = "TbRg";
            this.TbRg.Size = new System.Drawing.Size(206, 22);
            this.TbRg.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(17, 97);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 20);
            this.label10.TabIndex = 18;
            this.label10.Text = "RG";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(456, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 20);
            this.label11.TabIndex = 20;
            this.label11.Text = "Departamento*";
            // 
            // CbDepartamento
            // 
            this.CbDepartamento.FormattingEnabled = true;
            this.CbDepartamento.Location = new System.Drawing.Point(598, 96);
            this.CbDepartamento.Name = "CbDepartamento";
            this.CbDepartamento.Size = new System.Drawing.Size(206, 21);
            this.CbDepartamento.TabIndex = 21;
            // 
            // TbCargo
            // 
            this.TbCargo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbCargo.Location = new System.Drawing.Point(598, 123);
            this.TbCargo.MaxLength = 10;
            this.TbCargo.Name = "TbCargo";
            this.TbCargo.Size = new System.Drawing.Size(206, 22);
            this.TbCargo.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(456, 123);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 20);
            this.label12.TabIndex = 22;
            this.label12.Text = "Cargo";
            // 
            // TbEmailEmpresa
            // 
            this.TbEmailEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbEmailEmpresa.Location = new System.Drawing.Point(598, 213);
            this.TbEmailEmpresa.MaxLength = 30;
            this.TbEmailEmpresa.Name = "TbEmailEmpresa";
            this.TbEmailEmpresa.Size = new System.Drawing.Size(206, 22);
            this.TbEmailEmpresa.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(456, 213);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(136, 20);
            this.label13.TabIndex = 24;
            this.label13.Text = "Email da empresa";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(17, 210);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 20);
            this.label14.TabIndex = 26;
            this.label14.Text = "Sexo";
            // 
            // CbSexo
            // 
            this.CbSexo.FormattingEnabled = true;
            this.CbSexo.Location = new System.Drawing.Point(149, 212);
            this.CbSexo.Name = "CbSexo";
            this.CbSexo.Size = new System.Drawing.Size(206, 21);
            this.CbSexo.TabIndex = 28;
            // 
            // TbCracha
            // 
            this.TbCracha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbCracha.Location = new System.Drawing.Point(598, 155);
            this.TbCracha.MaxLength = 30;
            this.TbCracha.Name = "TbCracha";
            this.TbCracha.Size = new System.Drawing.Size(206, 22);
            this.TbCracha.TabIndex = 30;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(456, 155);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(125, 20);
            this.label15.TabIndex = 29;
            this.label15.Text = "Nome no crachá";
            // 
            // RegistroFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TbCracha);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.CbSexo);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.TbEmailEmpresa);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.TbCargo);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.CbDepartamento);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TbRg);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.DtAdmissao);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.DtNascimento);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TbSalario);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TbEndereco);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TbTelefone);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TbEmail);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TbCPF);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TbNome);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "RegistroFuncionario";
            this.Size = new System.Drawing.Size(923, 459);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TbNome;
        private System.Windows.Forms.TextBox TbCPF;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TbEmail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TbTelefone;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TbEndereco;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TbSalario;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker DtNascimento;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker DtAdmissao;
        private System.Windows.Forms.TextBox TbRg;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox CbDepartamento;
        private System.Windows.Forms.TextBox TbCargo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TbEmailEmpresa;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox CbSexo;
        private System.Windows.Forms.TextBox TbCracha;
        private System.Windows.Forms.Label label15;
    }
}
