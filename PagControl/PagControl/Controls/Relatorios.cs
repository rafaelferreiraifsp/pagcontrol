﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PagControl.Dto;
using PagControl.Util;

namespace PagControl.Controls
{
    public partial class Relatorios : UserControl
    {
        public Relatorios()
        {
            InitializeComponent();

            CbDepartamento.DataSource = Enum.GetValues(typeof(DepartamentoDto));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string message = "Relatorio gerado";
            var result = MessageBox.Show(message, "Relatórios");

            new GeradorRelatorio().GerarRelatorio();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string message = "Relatorio gerado";
            var result = MessageBox.Show(message, "Relatórios");

            var departamento = (DepartamentoDto)CbDepartamento.SelectedIndex;

            new GeradorRelatorio().GerarRelatorio(departamento);
        }
    }
}
