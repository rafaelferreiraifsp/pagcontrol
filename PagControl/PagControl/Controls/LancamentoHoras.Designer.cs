﻿namespace PagControl.Controls
{
    partial class LancamentoHoras
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.CbFuncionarios = new System.Windows.Forms.ComboBox();
            this.Funcionário = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.DtDataLancamento = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CbFuncionarios
            // 
            this.CbFuncionarios.FormattingEnabled = true;
            this.CbFuncionarios.Location = new System.Drawing.Point(25, 96);
            this.CbFuncionarios.Name = "CbFuncionarios";
            this.CbFuncionarios.Size = new System.Drawing.Size(334, 21);
            this.CbFuncionarios.TabIndex = 0;
            // 
            // Funcionário
            // 
            this.Funcionário.AutoSize = true;
            this.Funcionário.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Funcionário.Location = new System.Drawing.Point(21, 73);
            this.Funcionário.Name = "Funcionário";
            this.Funcionário.Size = new System.Drawing.Size(92, 20);
            this.Funcionário.TabIndex = 10;
            this.Funcionário.Text = "Funcionário";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 24);
            this.label1.TabIndex = 9;
            this.label1.Text = "Lançamento de horas";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button1.Location = new System.Drawing.Point(666, 409);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(245, 36);
            this.button1.TabIndex = 16;
            this.button1.Text = "Salvar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // DtDataLancamento
            // 
            this.DtDataLancamento.Location = new System.Drawing.Point(25, 181);
            this.DtDataLancamento.Name = "DtDataLancamento";
            this.DtDataLancamento.Size = new System.Drawing.Size(338, 20);
            this.DtDataLancamento.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 158);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 20);
            this.label2.TabIndex = 19;
            this.label2.Text = "Data";
            // 
            // LancamentoHoras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DtDataLancamento);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Funcionário);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CbFuncionarios);
            this.Name = "LancamentoHoras";
            this.Size = new System.Drawing.Size(923, 459);
            this.Load += new System.EventHandler(this.LancamentoHoras_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CbFuncionarios;
        private System.Windows.Forms.Label Funcionário;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker DtDataLancamento;
        private System.Windows.Forms.Label label2;
    }
}
