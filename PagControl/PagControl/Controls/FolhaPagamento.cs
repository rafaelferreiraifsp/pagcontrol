﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PagControl.Manejadores;
using PagControl.Repositorios;
using PagControl.Dto;
using PagControl.Util;

namespace PagControl.Controls
{
    public partial class FolhaPagamento : UserControl
    {
        private FuncionarioRepositorio _FuncionarioRepositorio;
        private List<InfoBasicaFuncionarioDto> _Funcionarios;

        public FolhaPagamento()
        {
            InitializeComponent();
        }

        public void PreencherCombo()
        {
            _FuncionarioRepositorio = new FuncionarioRepositorio();

            _Funcionarios = _FuncionarioRepositorio.ListarTodos();

            CbFuncionarios.Items.Clear();

            _Funcionarios.ForEach(x =>
            {
                CbFuncionarios.Items.Add(x);
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HoleriteRepositorio holeriteRepositorio = new HoleriteRepositorio();
            InfoBasicaFuncionarioDto funcionarioSelecionado = (InfoBasicaFuncionarioDto)CbFuncionarios.SelectedItem;

            if (funcionarioSelecionado == null)
            {
                string message = "Algum funcionário deve ser selecionado";
                var result = MessageBox.Show(message, "Folha de pagamento");

                return;
            }

            if(holeriteRepositorio.HoleriteExistente(funcionarioSelecionado.CodigoFuncionario, DateTime.Now))
            {
                string message = "Já existe um holerite referente a este mês para este funcionario";
                var result = MessageBox.Show(message, "Folha de pagamento");

                return;
            }

            ManejadorCalculoHolerite manejador = new ManejadorCalculoHolerite();
            var codigoHolerite = manejador.CalcularHolerite(funcionarioSelecionado.CodigoFuncionario);

            if (codigoHolerite != 0)
            {
                string message = "Holerite gerado";
                var result = MessageBox.Show(message, "Folha de pagamento");

                new GeradorDocumentoHolerite().GerarDocumentoHolerite(codigoHolerite);
            }
        }
    }
}
