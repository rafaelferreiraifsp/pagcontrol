﻿namespace PagControl.Controls
{
    partial class FolhaPagamento
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Funcionário = new System.Windows.Forms.Label();
            this.CbFuncionarios = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Folhas de Pagamento";
            // 
            // Funcionário
            // 
            this.Funcionário.AutoSize = true;
            this.Funcionário.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Funcionário.Location = new System.Drawing.Point(21, 73);
            this.Funcionário.Name = "Funcionário";
            this.Funcionário.Size = new System.Drawing.Size(92, 20);
            this.Funcionário.TabIndex = 12;
            this.Funcionário.Text = "Funcionário";
            // 
            // CbFuncionarios
            // 
            this.CbFuncionarios.FormattingEnabled = true;
            this.CbFuncionarios.Location = new System.Drawing.Point(25, 96);
            this.CbFuncionarios.Name = "CbFuncionarios";
            this.CbFuncionarios.Size = new System.Drawing.Size(334, 21);
            this.CbFuncionarios.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button1.Location = new System.Drawing.Point(25, 134);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(334, 32);
            this.button1.TabIndex = 17;
            this.button1.Text = "Gerar folha deste mês";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FolhaPagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Funcionário);
            this.Controls.Add(this.CbFuncionarios);
            this.Controls.Add(this.label1);
            this.Name = "FolhaPagamento";
            this.Size = new System.Drawing.Size(923, 459);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Funcionário;
        private System.Windows.Forms.ComboBox CbFuncionarios;
        private System.Windows.Forms.Button button1;
    }
}
