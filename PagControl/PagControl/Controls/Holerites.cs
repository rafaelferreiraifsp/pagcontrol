﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PagControl.Repositorios;
using PagControl.Dto;
using PagControl.Util;

namespace PagControl.Controls
{
    public partial class Holerites : UserControl
    {
        private FuncionarioRepositorio _FuncionarioRepositorio;
        private List<InfoBasicaFuncionarioDto> _Funcionarios;

        public Holerites()
        {
            InitializeComponent();

            DtPeriodo.Format = DateTimePickerFormat.Custom;
            DtPeriodo.CustomFormat = "MM/yyyy";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HoleriteRepositorio holeriteRepositorio = new HoleriteRepositorio();

            if (DtPeriodo.Value > DateTime.Now)
            {
                string message = "Uma data retroativa deve ser selecionada";

                var result = MessageBox.Show(message, "Folha de pagamento");

                return;
            }

            InfoBasicaFuncionarioDto funcionarioSelecionado = (InfoBasicaFuncionarioDto)CbFuncionarios.SelectedItem;

            if (funcionarioSelecionado == null)
            {
                string message = "Algum funcionário deve ser selecionado";
                var result = MessageBox.Show(message, "Folha de pagamento");

                return;
            }

            if (!holeriteRepositorio.HoleriteExistente(funcionarioSelecionado.CodigoFuncionario, DtPeriodo.Value))
            {
                string message = "Não existe um holerite referente a este mes";
                var result = MessageBox.Show(message, "Folha de pagamento");

                return;
            }

            var holerite = holeriteRepositorio.BuscarHoleritePeriodo(funcionarioSelecionado.CodigoFuncionario, DtPeriodo.Value);

            if (holerite != null)
            {
                string message = "Arquivo foi criado!";
                var result = MessageBox.Show(message, "Folha de pagamento");

                new GeradorDocumentoHolerite().GerarDocumentoHolerite(holerite.CodigoHolerite);
            }
        }

        public void PreencherCombo()
        {
            _FuncionarioRepositorio = new FuncionarioRepositorio();

            _Funcionarios = _FuncionarioRepositorio.ListarTodos();

            CbFuncionarios.Items.Clear();

            _Funcionarios.ForEach(x =>
            {
                CbFuncionarios.Items.Add(x);
            });
        }
    }
}
