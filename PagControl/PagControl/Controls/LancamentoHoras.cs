﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PagControl.Repositorios;
using PagControl.Dto;

namespace PagControl.Controls
{
    public partial class LancamentoHoras : UserControl
    {
        private FuncionarioRepositorio _FuncionarioRepositorio;
        private List<InfoBasicaFuncionarioDto> _Funcionarios;

        public LancamentoHoras()
        {
            InitializeComponent();
        }

        public void PreencherCombo()
        {
             _FuncionarioRepositorio = new FuncionarioRepositorio();

            _Funcionarios = _FuncionarioRepositorio.ListarTodos();

            CbFuncionarios.Items.Clear();

            _Funcionarios.ForEach(x =>
            {
                CbFuncionarios.Items.Add(x);
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LancamentoInputModelDto lancamentoInputModelDto = new LancamentoInputModelDto();
            LancamentoRepositorio lancamentoRepositorio = new LancamentoRepositorio();

            InfoBasicaFuncionarioDto funcionarioSelecionado = (InfoBasicaFuncionarioDto)CbFuncionarios.SelectedItem;

            if(funcionarioSelecionado == null)
            {
                string message = "Algum funcionário deve ser selecionado";
                var result = MessageBox.Show(message, "Lançamento");

                return;
            }

            if (DtDataLancamento.Value.Date > DateTime.Now.Date)
            {
                string message = "Você não pode salvar um lançar um dia trabalhado a frente";
                var result = MessageBox.Show(message, "Lançamento");

                return;
            }

            lancamentoInputModelDto.CodigoFuncionario = funcionarioSelecionado.CodigoFuncionario;
            lancamentoInputModelDto.DataLancamento = DtDataLancamento.Value;

            if (!lancamentoRepositorio.LancamentoExistente(lancamentoInputModelDto))
            {
                if (lancamentoRepositorio.RegistrarLancamento(lancamentoInputModelDto))
                {
                    string message = "Lançamento registrado";
                    var result = MessageBox.Show(message, "Lançamento");
                }
                else
                {
                    string message = "Não foi possível cadastrar lançamento";
                    var result = MessageBox.Show(message, "Lançamento");
                }
            }
            else
            {
                string message = "Já existe um lançamento para este funcionário nessa data";
                var result = MessageBox.Show(message, "Lançamento");
            }
        }

        private void LancamentoHoras_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                PreencherCombo();
            }
        }
    }
}
