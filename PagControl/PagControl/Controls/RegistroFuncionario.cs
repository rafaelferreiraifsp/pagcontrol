﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PagControl.Dto;
using PagControl.Util;
using PagControl.Repositorios;

namespace PagControl.Controls
{
    public partial class RegistroFuncionario : UserControl
    {
        public RegistroFuncionario()
        {
            InitializeComponent();

            CbDepartamento.DataSource = Enum.GetValues(typeof(DepartamentoDto));
            CbSexo.DataSource = Enum.GetValues(typeof(SexoDto));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FuncionarioRepositorio _FuncionarioRepositorio = new FuncionarioRepositorio();

            FuncionarioInputDto novoFuncionario = new FuncionarioInputDto();

            if (string.IsNullOrEmpty(TbEndereco.Text) || string.IsNullOrEmpty(TbNome.Text) || string.IsNullOrEmpty(TbTelefone.Text) || string.IsNullOrEmpty(TbSalario.Text))
            {
                string message = "Preencha todos os dados";
                var result = MessageBox.Show(message, "Validação");

                return;
            }

            if (!Validador.ValidarCpf(TbCPF.Text))
            {
                string message = "CPF inválido";
                var result = MessageBox.Show(message, "Validação");

                return;
            }

            if (!Validador.ValidarEmail(TbEmail.Text))
            {
                string message = "Email inválido";
                var result = MessageBox.Show(message, "Validação");

                return;
            }

            if (_FuncionarioRepositorio.CpfExistente(TbCPF.Text))
            {
                string message = "Já existe um funcionário com este CPF";
                var result = MessageBox.Show(message, "Validação");

                return;
            }

            var isNumeric = int.TryParse(TbSalario.Text, out int n);

            if (isNumeric)
                novoFuncionario.Salario = double.Parse(TbSalario.Text);
            else
            {
                string message = "Salário inválido";
                var result = MessageBox.Show(message, "Validação");

                return;
            }

            if(Validador.CalcularIdade(DtNascimento.Value) < 18)
            {
                string message = "O funcionário deve ter 18 anos ou mais";
                var result = MessageBox.Show(message, "Validação");

                return;
            }

            novoFuncionario.DataNascimento = DtNascimento.Value;
            novoFuncionario.Cpf = TbCPF.Text;
            novoFuncionario.EmailPessoal = TbEmail.Text;
            novoFuncionario.Endereco = TbEndereco.Text;
            novoFuncionario.Nome = TbNome.Text;
            novoFuncionario.Telefone = TbTelefone.Text;
            novoFuncionario.RG = TbRg.Text;
            novoFuncionario.DataAdmissao = DtAdmissao.Value;
     
            novoFuncionario.Cargo = TbCargo.Text;
            novoFuncionario.EmailEmpresa = TbEmailEmpresa.Text;
            novoFuncionario.NomeCracha = TbCracha.Text;
            novoFuncionario.Departamento = (DepartamentoDto)CbDepartamento.SelectedIndex;
            novoFuncionario.Sexo = (SexoDto)CbSexo.SelectedIndex;

            if (_FuncionarioRepositorio.CadastrarFuncionario(novoFuncionario))
            {
                string message = "Funcionário foi cadastrado!";
                var result = MessageBox.Show(message, "Cadastro");

                LimparCampos();
            }
            else
            {
                string message = "Não foi possível cadastrar funcionário";
                var result = MessageBox.Show(message, "Cadastro");
            }
        }

        public void LimparCampos()
        {
            TbCPF.Text = "";
            TbEndereco.Text = "";
            TbNome.Text = "";
            TbTelefone.Text = "";
            TbSalario.Text = "";
            TbEmail.Text = "";
            TbRg.Text = "";
            TbCracha.Text = "";
            TbCargo.Text = "";
            TbEmailEmpresa.Text = "";
        } 
    }
}
