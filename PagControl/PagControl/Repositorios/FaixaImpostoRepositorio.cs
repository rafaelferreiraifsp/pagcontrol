﻿using Npgsql;
using PagControl.Dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Repositorios
{
    public class FaixaImpostoRepositorio
    {
        public List<FaixaImpostoDto> ListarFaixas(int codigoImposto)
        {
            List<FaixaImpostoDto> ListaResultado = new List<FaixaImpostoDto>();

            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT codigo, valor_faixa, condical, deduzir_valor FROM faixa_imposto WHERE codigo_imposto=@cod ORDER BY condical", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@cod", codigoImposto);
                    NpgsqlDataReader reader;

                    reader = scmd.ExecuteReader();

                    while (reader.Read())
                    {
                        FaixaImpostoDto newFaixa = new FaixaImpostoDto();

                        newFaixa.CodigoFaixa = (int)reader[0];
                        newFaixa.ValorFaixa = Convert.ToDouble(reader[1]);
                        newFaixa.Condicao = Convert.ToDouble(reader[2]);

                        if (!(reader[3] is DBNull))
                            newFaixa.DeduzirValor = Convert.ToDouble(reader[3]);

                        ListaResultado.Add(newFaixa);
                    }

                    return ListaResultado;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }
    }
}
