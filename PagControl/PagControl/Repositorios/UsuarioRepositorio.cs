﻿using Npgsql;
using PagControl.App;
using PagControl.Dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Repositorios
{
    public class UsuarioRepositorio
    {
        public bool Login(CredenciaisUsuarioDto credenciais)
        {
            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT nome, codigo, perfil FROM usuario WHERE email=@eml and senha=@sen", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@eml", credenciais.Email);
                    scmd.Parameters.AddWithValue("@sen", Util.Encriptacao.CalcularSHA1(credenciais.Senha));

                    NpgsqlDataReader reader;

                    reader = scmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            UsuarioLogado.NomeUsuario = (string)reader[0];
                            UsuarioLogado.CodigoUsuario = (int)reader[1];
                            UsuarioLogado.DataExpiracao = DateTime.Now.AddMinutes(30);
                            UsuarioLogado.Perfil = (Perfil)(int)reader[2];
                        }

                        return true;
                    }
                    return false;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }
    }
}
