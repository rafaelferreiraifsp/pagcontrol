﻿using Npgsql;
using PagControl.Dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Repositorios
{
    public class ImpostoAplicadoRepositorio
    {
        public bool CadastrarImpostosAplicados(List<ImpostoAplicadoDto> impostos)
        {
            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                impostos.ForEach(x =>
                {
                    using (pgsqlConnection = new NpgsqlConnection(strConexao))
                    {

                        pgsqlConnection.Open();

                        NpgsqlCommand scmd = new NpgsqlCommand("INSERT INTO imposto_aplicado (valor, percentual, nome_imposto,codigo_holerite) VALUES (@val, @perc, @nomeIm, @codH)", pgsqlConnection);
                        scmd.Parameters.Clear();
                        scmd.Parameters.AddWithValue("@val", x.Valor);
                        scmd.Parameters.AddWithValue("@perc", x.Percentual);
                        scmd.Parameters.AddWithValue("@nomeIm", x.NomeImposto);
                        scmd.Parameters.AddWithValue("@codH", x.CodigoHolerite);

                        scmd.ExecuteNonQuery();
                    }
                });

                return true;
            }
            catch (NpgsqlException ex)
            {
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }

        public List<ImpostoAplicadoDto> ListarImpostosAplicados(int codigoHolerite)
        {
            List<ImpostoAplicadoDto> impostosAplicados = new List<ImpostoAplicadoDto>();

            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT codigo, valor, percentual, nome_imposto, codigo_holerite FROM imposto_aplicado WHERE codigo_holerite = @codH", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@codH", codigoHolerite);
                    NpgsqlDataReader reader;

                    reader = scmd.ExecuteReader();

                    while (reader.Read())
                    {
                        impostosAplicados.Add(new ImpostoAplicadoDto()
                        {
                            Codigo = (int)reader[0],
                            Valor = Convert.ToDouble(reader[1]),
                            Percentual = Convert.ToDouble(reader[2]),
                            NomeImposto = (string)reader[3],
                            CodigoHolerite = (int)reader[4]
                        });
                    }

                    return impostosAplicados;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }
    }
}
