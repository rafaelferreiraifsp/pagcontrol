﻿using Npgsql;
using PagControl.Dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Repositorios
{
    public class LancamentoRepositorio
    {
        public bool LancamentoExistente(LancamentoInputModelDto lancamentoInputModel)
        {
            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT count (*) AS qtd FROM lancamento WHERE data_lancamento=@data AND codigo_funcionario=@funcionario", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@data", lancamentoInputModel.DataLancamento.Date);
                    scmd.Parameters.AddWithValue("@funcionario", lancamentoInputModel.CodigoFuncionario);

                    if (scmd.ExecuteScalar().ToString() == "1")
                        return true;

                    return false;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }

        public int ListarDiasTrabalhados(DateTime mesInicio, DateTime mesFim, int codigoFuncionario)
        {
            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;
            int diasTrabalhados = 0;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT count(codigo) FROM lancamento WHERE codigo_funcionario=@codig AND data_lancamento > @mesInicio AND data_lancamento < @mesFim", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@codig", codigoFuncionario);
                    scmd.Parameters.AddWithValue("@mesInicio", mesInicio.Date);
                    scmd.Parameters.AddWithValue("@mesFim", mesFim.Date);

                    NpgsqlDataReader reader = scmd.ExecuteReader();

                    while (reader.Read())
                    {
                        diasTrabalhados = reader.GetInt32(0);
                    }

                    return diasTrabalhados;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }


        public bool RegistrarLancamento(LancamentoInputModelDto lancamentoInputModel)
        {
            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("INSERT INTO lancamento (data_lancamento, codigo_funcionario) VALUES (@data, @funcionario)", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@data", lancamentoInputModel.DataLancamento);
                    scmd.Parameters.AddWithValue("@funcionario", lancamentoInputModel.CodigoFuncionario);

                    if (scmd.ExecuteNonQuery().ToString() == "1")
                        return true;

                    return false;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }
    }
}
