﻿using Npgsql;
using PagControl.Dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Repositorios
{
    public class FuncionarioRepositorio
    {
        public bool CpfExistente(string cpf)
        {
            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT count (*) AS qtd FROM funcionario WHERE cpf=@cpf", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@cpf", cpf);

                    if (scmd.ExecuteScalar().ToString() == "1")
                        return true;

                    return false;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }

        public FuncionarioDto BuscarFuncionario(int codigoFuncionario)
        {
            FuncionarioDto funcionarioDto = new FuncionarioDto();

            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT codigo, salario, nome, cpf, email_pessoal, telefone, endereco, rg, dataadmissao, datanascimento FROM funcionario where codigo = @cod", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@cod", codigoFuncionario);
                    NpgsqlDataReader reader;

                    reader = scmd.ExecuteReader();

                    while (reader.Read())
                    {
                        funcionarioDto.CodigoFuncionario = (int)reader[0];
                        funcionarioDto.Salario = Convert.ToDouble(reader[1]);
                        funcionarioDto.NomeFuncionario = (string)reader[2];
                        funcionarioDto.CPF = (string)reader[3];
                        funcionarioDto.Email = (string)reader[4];
                        funcionarioDto.Telefone = (string)reader[5];
                        funcionarioDto.Endereco = (string)reader[6];
                        funcionarioDto.RG = (string)reader[7];
                        funcionarioDto.DataAdmissao = (DateTime)reader[8];
                        funcionarioDto.DataNascimento = (DateTime)reader[9];
                    }

                    return funcionarioDto;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }

        public bool CadastrarFuncionario(FuncionarioInputDto funcionario)
        {
            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("INSERT INTO funcionario (salario, nome, cpf, email_pessoal, telefone, endereco, rg, dataAdmissao, datanascimento, departamento, email_empresa, sexo, cargo, nome_cracha) " +
                        "VALUES (@salario, @nome, @cpf, @email, @telefone, @endereco, @rg, @dataA, @dataN, @dept, @emlEmpr, @sexo, @car, @nocracha)", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@salario", funcionario.Salario);
                    scmd.Parameters.AddWithValue("@nome", funcionario.Nome);
                    scmd.Parameters.AddWithValue("@cpf", funcionario.Cpf);
                    scmd.Parameters.AddWithValue("@email", funcionario.EmailPessoal);
                    scmd.Parameters.AddWithValue("@telefone", funcionario.Telefone);
                    scmd.Parameters.AddWithValue("@endereco", funcionario.Endereco);
                    scmd.Parameters.AddWithValue("@rg", funcionario.RG);
                    scmd.Parameters.AddWithValue("@dataA", funcionario.DataAdmissao);
                    scmd.Parameters.AddWithValue("@dataN", funcionario.DataNascimento);
                    scmd.Parameters.AddWithValue("@dept", (int)funcionario.Departamento);
                    scmd.Parameters.AddWithValue("@emlEmpr", funcionario.EmailEmpresa);
                    scmd.Parameters.AddWithValue("@sexo", (int)funcionario.Sexo);
                    scmd.Parameters.AddWithValue("@car", funcionario.Cargo);
                    scmd.Parameters.AddWithValue("@nocracha", funcionario.NomeCracha);

                    if (scmd.ExecuteNonQuery().ToString() == "1")
                        return true;

                    return false;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }

        public List<InfoBasicaFuncionarioDto> ListarTodos()
        {
            List<InfoBasicaFuncionarioDto> ListaResultado = new List<InfoBasicaFuncionarioDto>();

            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT codigo, nome FROM funcionario", pgsqlConnection);
                    scmd.Parameters.Clear();
                    NpgsqlDataReader reader;

                    reader = scmd.ExecuteReader();

                    while (reader.Read())
                    {
                        ListaResultado.Add(new InfoBasicaFuncionarioDto()
                        {
                            CodigoFuncionario = (int)reader[0],
                            Nome = (string)reader[1]
                        });
                    }

                    return ListaResultado;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }
    }
}
