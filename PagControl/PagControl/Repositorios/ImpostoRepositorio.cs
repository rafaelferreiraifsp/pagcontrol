﻿using Npgsql;
using PagControl.Dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Repositorios
{
    public class ImpostoRepositorio
    {
        public List<ImpostoDto> ListarImpostos()
        {
            List<ImpostoDto> ListaResultado = new List<ImpostoDto>();

            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT codigo, nome_imposto, tem_faixas, imposto_relacionado FROM imposto", pgsqlConnection);
                    scmd.Parameters.Clear();
                    NpgsqlDataReader reader;

                    reader = scmd.ExecuteReader();

                    while (reader.Read())
                    {
                        ImpostoDto newImpostoDto = new ImpostoDto();

                        newImpostoDto.CodigoImposto = (int)reader[0];
                        newImpostoDto.Nome = (string)reader[1];
                        newImpostoDto.TemFaixas = (bool)reader[2];

                        if(!(reader[3] is DBNull))
                            newImpostoDto.CodigoImpostoRelacionado = Convert.ToInt32(reader[3]);

                        ListaResultado.Add(newImpostoDto);
                    }

                    return ListaResultado;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }
    }
}
