﻿using Npgsql;
using PagControl.Dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagControl.Repositorios
{
    public class HoleriteRepositorio
    {
        public int CadastrarHolerite(HoleriteDto holerite)
        {
            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("INSERT INTO holerite (total_descontos, valor_liquido, total_vencimentos, dias_trabalhados, mes_referente, codigo_funcionario) VALUES (@totalDesc, @valorLiq, @totalVen, @diasT, @mesR, @codF) RETURNING codigo ", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@totalDesc", holerite.TotalDescontos);
                    scmd.Parameters.AddWithValue("@valorLiq", holerite.ValorLiquido);
                    scmd.Parameters.AddWithValue("@totalVen", holerite.TotalVencimentos);
                    scmd.Parameters.AddWithValue("@diasT", holerite.DiasTrabalhados);
                    scmd.Parameters.AddWithValue("@mesR", holerite.MesReferente);
                    scmd.Parameters.AddWithValue("@codF", holerite.CodigoFuncionario);

                    NpgsqlDataReader reader = scmd.ExecuteReader();

                    int codigoHolerite = 0;

                    while (reader.Read())
                    {
                        codigoHolerite = reader.GetInt32(0);
                    }

                    return codigoHolerite;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }

        public bool HoleriteExistente(int codFuncionario, DateTime mesReferente)
        {
            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT count (*) AS qtd FROM holerite WHERE codigo_funcionario=@codF AND date_part('year', mes_referente) = @anoR AND date_part('month', mes_referente) = @mesR", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@codF", codFuncionario);
                    scmd.Parameters.AddWithValue("@anoR", mesReferente.Year);
                    scmd.Parameters.AddWithValue("@mesR", mesReferente.Month);

                    if (scmd.ExecuteScalar().ToString() == "1")
                        return true;

                    return false;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }

        public List<RelatorioItemDto> BuscarRelatorio(DepartamentoDto dpt, DateTime mesReferente)
        {
            List<RelatorioItemDto> RelatorioItens = new List<RelatorioItemDto>();

            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT funcionario.departamento, funcionario.nome, funcionario.cpf, holerite.total_vencimentos," +
                        " holerite.valor_liquido, holerite.total_descontos FROM holerite INNER JOIN funcionario on " +
                        "codigo_funcionario = funcionario.codigo where date_part('year', mes_referente) = @anoR AND date_part('month', mes_referente) = @mesR AND funcionario.departamento=@dpt", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@anoR", mesReferente.Year);
                    scmd.Parameters.AddWithValue("@mesR", mesReferente.Month);
                    scmd.Parameters.AddWithValue("@dpt", (int)dpt);
                    NpgsqlDataReader reader;

                    reader = scmd.ExecuteReader();

                    while (reader.Read())
                    {
                        RelatorioItemDto relatorioItemDto = new RelatorioItemDto();

                        relatorioItemDto.Departamento = (DepartamentoDto)(int)reader[0];
                        relatorioItemDto.NomeFuncionario = (string)reader[1];
                        relatorioItemDto.CPF = (string)reader[2];
                        relatorioItemDto.TotalVencimentos = Convert.ToDouble(reader[3]);
                        relatorioItemDto.SalarioLiquido = Convert.ToDouble(reader[4]);
                        relatorioItemDto.TotalDescontos = Convert.ToDouble(reader[5]);

                        RelatorioItens.Add(relatorioItemDto);
                    }

                    return RelatorioItens;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }

        public List<RelatorioItemDto> BuscarRelatorio(DateTime mesReferente)
        {
            List<RelatorioItemDto> RelatorioItens = new List<RelatorioItemDto>();

            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT funcionario.departamento, funcionario.nome, funcionario.cpf, holerite.total_vencimentos," +
                        " holerite.valor_liquido, holerite.total_descontos FROM holerite INNER JOIN funcionario on " +
                        "codigo_funcionario = funcionario.codigo where date_part('year', mes_referente) = @anoR AND date_part('month', mes_referente) = @mesR", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@anoR", mesReferente.Year);
                    scmd.Parameters.AddWithValue("@mesR", mesReferente.Month);
                    NpgsqlDataReader reader;

                    reader = scmd.ExecuteReader();

                    while (reader.Read())
                    {
                        RelatorioItemDto relatorioItemDto = new RelatorioItemDto();

                        relatorioItemDto.Departamento = (DepartamentoDto)(int)reader[0];
                        relatorioItemDto.NomeFuncionario = (string)reader[1];
                        relatorioItemDto.CPF = (string)reader[2];
                        relatorioItemDto.TotalVencimentos = Convert.ToDouble(reader[3]);
                        relatorioItemDto.SalarioLiquido = Convert.ToDouble(reader[4]);
                        relatorioItemDto.TotalDescontos = Convert.ToDouble(reader[5]);

                        RelatorioItens.Add(relatorioItemDto);
                    }

                    return RelatorioItens;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }

        public HoleriteDto BuscarHolerite(int codHolerite)
        {
            HoleriteDto holeriteDto = new HoleriteDto();

            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT codigo, total_descontos, valor_liquido, total_vencimentos, dias_trabalhados, mes_referente, codigo_funcionario FROM holerite WHERE codigo=@codH", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@codH", codHolerite);
                    NpgsqlDataReader reader;

                    reader = scmd.ExecuteReader();

                    while (reader.Read())
                    {
                        holeriteDto.CodigoHolerite = (int)reader[0];
                        holeriteDto.TotalDescontos= Convert.ToDouble(reader[1]);
                        holeriteDto.ValorLiquido= Convert.ToDouble(reader[2]);
                        holeriteDto.TotalVencimentos = Convert.ToDouble(reader[3]);
                        holeriteDto.DiasTrabalhados = (int)reader[4];
                        holeriteDto.MesReferente = (DateTime)reader[5];
                        holeriteDto.CodigoHolerite = (int)reader[6];
                    }

                    return holeriteDto;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }

        public HoleriteDto BuscarHoleritePeriodo(int codFuncionario, DateTime mesReferente)
        {
            HoleriteDto holeriteDto = new HoleriteDto();

            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT codigo, total_descontos, valor_liquido, total_vencimentos," +
                        " dias_trabalhados, mes_referente, codigo_funcionario FROM holerite" +
                        " WHERE codigo_funcionario=@codF AND date_part('year', mes_referente) = @anoR AND date_part('month', mes_referente) = @mesR", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@codF", codFuncionario);
                    scmd.Parameters.AddWithValue("@anoR", mesReferente.Year);
                    scmd.Parameters.AddWithValue("@mesR", mesReferente.Month);
                    NpgsqlDataReader reader;

                    reader = scmd.ExecuteReader();

                    while (reader.Read())
                    {
                        holeriteDto.CodigoHolerite = (int)reader[0];
                        holeriteDto.TotalDescontos = Convert.ToDouble(reader[1]);
                        holeriteDto.ValorLiquido = Convert.ToDouble(reader[2]);
                        holeriteDto.TotalVencimentos = Convert.ToDouble(reader[3]);
                        holeriteDto.DiasTrabalhados = (int)reader[4];
                        holeriteDto.MesReferente = (DateTime)reader[5];
                        holeriteDto.CodigoFuncionario = (int)reader[6];
                    }

                    return holeriteDto;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }

        public HoleriteCompletoDto BuscarHoleriteMaisInfo(int codHolerite)
        {
            HoleriteCompletoDto holeriteCompletoDto = new HoleriteCompletoDto();

            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT holerite.codigo, total_descontos, valor_liquido, total_vencimentos, dias_trabalhados, mes_referente, codigo_funcionario, funcionario.nome, funcionario.departamento FROM holerite INNER JOIN funcionario on codigo_funcionario = funcionario.codigo WHERE holerite.codigo = @codH", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@codH", codHolerite);
                    NpgsqlDataReader reader;

                    reader = scmd.ExecuteReader();

                    while (reader.Read())
                    {
                        holeriteCompletoDto.CodigoHolerite = (int)reader[0];
                        holeriteCompletoDto.TotalDescontos = Convert.ToDouble(reader[1]);
                        holeriteCompletoDto.ValorLiquido = Convert.ToDouble(reader[2]);
                        holeriteCompletoDto.TotalVencimentos = Convert.ToDouble(reader[3]);
                        holeriteCompletoDto.DiasTrabalhados = (int)reader[4];
                        holeriteCompletoDto.MesReferente = (DateTime)reader[5];
                        holeriteCompletoDto.CodigoFuncionario = (int)reader[6];
                        holeriteCompletoDto.NomeFuncionario = (string)reader[7];
                        holeriteCompletoDto.Departamento = (DepartamentoDto)(int)reader[8];
                    }

                    return holeriteCompletoDto;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }
    }
}
