﻿using PagControl.App;
using PagControl.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PagControl
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            InitializeComponent();

            if (UsuarioLogado.Perfil == Perfil.Auxiliar)
            {
                button5.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            registroFuncionario1.BringToFront();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lancamentoHoras1.PreencherCombo();
            lancamentoHoras1.BringToFront();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            holerites1.PreencherCombo();
            holerites1.BringToFront();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            folhaPonto1.BringToFront();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            folhaPagamento2.PreencherCombo();
            folhaPagamento2.BringToFront();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Hide();
            Form1 login = new Form1();
            login.ShowDialog();
            Close();
        }
    }
}
