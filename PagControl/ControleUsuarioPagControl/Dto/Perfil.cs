﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleUsuarioPagControl.Dto
{
    public enum Perfil
    {
        Coordenador,
        Auxiliar
    }
}
