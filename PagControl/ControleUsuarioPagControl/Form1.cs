﻿using ControleUsuarioPagControl.Dto;
using ControleUsuarioPagControl.Util;
using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControleUsuarioPagControl
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            CbPerfil.DataSource = Enum.GetValues(typeof(Perfil));
        }

        private bool UsuarioExistente(string email)
        {
            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {

                    pgsqlConnection.Open();

                    NpgsqlCommand scmd = new NpgsqlCommand("SELECT nome, codigo, perfil FROM usuario WHERE email=@eml", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@eml", email);

                    NpgsqlDataReader reader;

                    reader = scmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }

        private void ButtonSalvar_Click(object sender, EventArgs e)
        {
            var nome = TbNome.Text;
            var sobrenome = TbSobrenome.Text;
            var email = TbEmail.Text;
            var senha = Encriptacao.CalcularSHA1(TbSenha.Text);
            var perfil = (Perfil)CbPerfil.SelectedIndex;

            if(string.IsNullOrEmpty(nome) || string.IsNullOrEmpty(sobrenome) || string.IsNullOrEmpty(email) || string.IsNullOrEmpty(TbSenha.Text))
            {
                string message = "Preencha todos os dados";
                var result = MessageBox.Show(message);

                return;
            }
            if (!Validador.ValidarEmail(email))
            {
                string message = "E-mail inválido";
                var result = MessageBox.Show(message);

                return;
            }
            if(UsuarioExistente(email))
            {
                string message = "Já existe um usuário com este e-mail";
                var result = MessageBox.Show(message);

                return;
            }

            string strConexao = ConfigurationManager.ConnectionStrings["ConexaoPostgres"].ConnectionString;

            NpgsqlConnection pgsqlConnection = null;

            try
            {
                using (pgsqlConnection = new NpgsqlConnection(strConexao))
                {
                    pgsqlConnection.Open();
                    NpgsqlCommand scmd = new NpgsqlCommand("INSERT INTO usuario (nome, sobrenome, email, senha, perfil) VALUES (@nome, @sobrenome, @email, @senha, @perfil)", pgsqlConnection);
                    scmd.Parameters.Clear();
                    scmd.Parameters.AddWithValue("@nome", nome);
                    scmd.Parameters.AddWithValue("@sobrenome", sobrenome);
                    scmd.Parameters.AddWithValue("@email", email);
                    scmd.Parameters.AddWithValue("@senha", senha);
                    scmd.Parameters.AddWithValue("@perfil", (int)perfil);

                    if (scmd.ExecuteNonQuery().ToString() == "1")
                    {
                        const string message = "Usuário inserido";
                        var result = MessageBox.Show(message);
                    }
                }
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                pgsqlConnection.Close();
            }
        }
    }
}
